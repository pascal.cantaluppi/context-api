import React, { useState, useContext } from "react";
import { MovieContext } from "./MovieContext";

const AddMovie = () => {
  const [movies, setMovies] = useContext(MovieContext);
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");

  const updateName = e => {
    setName(e.target.value);
  };

  const updatePrice = e => {
    setPrice(e.target.value);
  };

  const addMovie = e => {
    e.preventDefault();
    setMovies(prevMovies => [...prevMovies, { name: name, price: price }]);
  };

  return (
    <form onSubmit={addMovie}>
      <p>
        <hr />
      </p>
      <p>
        <i>Add Movie</i>
      </p>
      <p>
        <input type="text" name="name" value={name} onChange={updateName} />
      </p>
      <p>
        <input type="text" name="price" value={price} onChange={updatePrice} />
      </p>
      <p>
        <button>Submit</button>
      </p>
    </form>
  );
};

export default AddMovie;
