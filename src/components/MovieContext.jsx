import React, { useState, createContext } from "react";

export const MovieContext = createContext();

export const MovieProvider = props => {
  const [movies, setMovies] = useState([
    {
      id: 123,
      name: "Joker",
      price: "CHF 20.00"
    },
    {
      id: 456,
      name: "Inception",
      price: "CHF 20.00"
    },
    {
      id: 789,
      name: "IT Part II",
      price: "CHF 20.00"
    }
  ]);

  return (
    <MovieContext.Provider value={[movies, setMovies]}>
      {props.children}
    </MovieContext.Provider>
  );
};
