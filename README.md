# React Context API

<p>
  <a href="https://reactjs.org/docs/context.html#when-to-use-context" target="blank"><img src="https://gitlab.com/pascal.cantaluppi/context-api/raw/master/public/react.png" width="100" alt="React Logo" />
</p>

## React Context API

<p>Context provides a way to pass data through the component tree without having to pass props down manually at every level.</p>

- <a href="https://reactjs.org/docs/context.html#when-to-use-context" target="blank">https://reactjs.org/docs/context.html#when-to-use-context</a>
